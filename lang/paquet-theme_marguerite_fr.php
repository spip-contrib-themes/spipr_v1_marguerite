<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_marguerite_description' => 'Du vert et du jaune-orange.',
	'theme_marguerite_slogan' => 'Du vert et du jaune-orange',
);
